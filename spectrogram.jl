# Copyright (c) 2014, Nick Palmius (University of Oxford)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the University of Oxford nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Contact: npalmius@googlemail.com
# Originally written by Nick Palmius, 1-Oct-2014

function spectrogram(f::Function)
    spectrogram(f, Dict());
end

function spectrogram(f::Function, opts::Dict)
    t_min = -5;
    t_max = 5;
    dt = 0.05;

    # Process options
    if haskey(opts, "t_min");  t_min = opts["t_min"];  end;
    if haskey(opts, "t_max");  t_max = opts["t_max"];  end;
    if haskey(opts, "dt");     dt = opts["dt"];        end;

    opts["t_min"] = t_min;
    opts["t_max"] = t_max;
    opts["dt"] = dt;

    t_range = t_min:dt:t_max;

    f_vals = zeros(length(t_range)) + im;
    for i = 1:length(t_range)
        f_vals[i] = f(t_range[i]);
    end

    return spectrogram(f_vals, opts);
end

function spectrogram(s::Array, opts::Dict)
    t_min = NaN;
    t_max = NaN;
    dt = NaN;

    fft_window = 100;

    if haskey(opts, "t_min");  t_min = opts["t_min"];  end;
    if haskey(opts, "t_max");  t_max = opts["t_max"];  end;
    if haskey(opts, "dt");     dt    = opts["dt"];     end;
    if haskey(opts, "fft_window_size"); fft_window = opts["fft_window_size"]; end;

    if isnan(t_min) && isnan(t_max) && isnan(dt)
        t_min = 0;
        t_max = length(s) - 1;
        dt = 1;
    elseif !isnan(dt)
        if isnan(t_min) && isnan(t_max)
            t_min = 0;
            t_max = (length(s) - 1) * dt;
        elseif isnan(t_max)
            t_max = ((length(s) - 1) * dt) + t_min;
        elseif isnan(t_min)
            t_min = t_max - ((length(s) - 1) * dt);
        end
    else
        if isnan(t_max)
            t_max = t_min + length(s) - 1;
        elseif isnan(t_min)
            t_min = t_max - length(s) + 1;
        end
        dt = (t_max - t_min) / (length(s) - 1);
    end

    ω_min = 0;
    ω_max = 1 / dt;
    dω = 1 / (fft_window * dt);
    ω_range = ω_min:dω:ω_max;

    t_range = t_min:dt:t_max;

    W = zeros(length(s), fft_window + 1) + 0im;

    for i = 1:length(s)
        r = max(1, i - int64(floor(fft_window / 2))):min(length(s), i + int64(ceil(fft_window / 2)))
        r_vals = zeros(fft_window + 1) + 0im;
        r_start = r[1] - (i - int64(floor(fft_window / 2))) + 1
        r_end = r_start + length(r) - 1;
        r_vals[r_start:r_end] = s[r];
        r_vals .*= (0.5 * (1 - cos((2 * π * [0:fft_window]) / fft_window)))
        W[i,:] = fft(r_vals);
    end

    return (W, t_range, ω_range);
end