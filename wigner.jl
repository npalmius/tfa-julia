# Copyright (c) 2014, Nick Palmius (University of Oxford)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the University of Oxford nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Contact: npalmius@googlemail.com
# Originally written by Nick Palmius, 1-Oct-2014

function wigner(f::Function)
	wigner(f, Dict());
end

function wigner(f::Function, opts::Dict)
	t_min = -5;
	t_max = 5;
	dt = 0.1;

	ω_min = 0;
	ω_max = 10;
	dω = 0.05;

	t_diff = t_max - t_min
	τ_min = -t_diff;
	τ_max = t_diff;
	dτ = 0.01;

    # Process options
    if haskey(opts, "t_min");  t_min = opts["t_min"];  end;
    if haskey(opts, "t_max");  t_max = opts["t_max"];  end;
    if haskey(opts, "dt");     dt    = opts["dt"];     end;
    if haskey(opts, "ω_min");  ω_min = opts["ω_min"];  end;
    if haskey(opts, "ω_max");  ω_max = opts["ω_max"];  end;
    if haskey(opts, "dω");     dω    = opts["dω"];     end;
    if haskey(opts, "τ_min");  τ_min = opts["τ_min"];  end;
    if haskey(opts, "τ_max");  τ_max = opts["τ_max"];  end;
    if haskey(opts, "dτ");     dτ    = opts["dτ"];     end;

	t_range = t_min:dt:t_max;
	ω_range = ω_min:dω:ω_max;
	τ_range = τ_min:dτ:τ_max;

	W = zeros(length(t_range), length(ω_range)) + 0im;

	i = 1;
	for t = t_range
		j = 1;
		for ω = ω_range
			f_vals = zeros(length(τ_range)) + 0im;
			k = 1;
			for τ = τ_range
				f_vals[k] = conj(f(t - (τ/2))) * f(t + (τ/2)) * exp(-im * τ * 2 * π * ω)
				k += 1;
			end
			W[i, j] = (dτ / 2) * (f_vals[1] + f_vals[length(τ_range)] + (2 * (sum(f_vals[2:(length(τ_range)-1)]))))
			j += 1;
		end
		i += 1;
	end

	return (W, t_range, ω_range);
end

function wigner(s::Array{Float64,1})
	wigner(s + 0im, Dict());
end

function wigner(s::Array{Float64,1}, opts::Dict)
	wigner(s + 0im, opts);
end

function wigner(s::Array{Complex{Float64},1})
	wigner(s, Dict());
end

function wigner(s::Array{Complex{Float64},1}, opts::Dict)
    t_min::Float64 = NaN;
    t_max::Float64 = NaN;
    dt::Float64 = NaN;

	ω_min::Float64 = 0.;
	ω_max::Float64 = 10.;
	dω::Float64 = 0.05;

    # Process options
    if haskey(opts, "t_min");  t_min = opts["t_min"];  end;
    if haskey(opts, "t_max");  t_max = opts["t_max"];  end;
    if haskey(opts, "dt");     dt    = opts["dt"];     end;
    if haskey(opts, "ω_min");  ω_min = opts["ω_min"];  end;
    if haskey(opts, "ω_max");  ω_max = opts["ω_max"];  end;
    if haskey(opts, "dω");     dω    = opts["dω"];     end;

    if isnan(t_min) && isnan(t_max) && isnan(dt)
        t_min = 0;
        t_max = length(s) - 1;
        dt = 1;
    elseif !isnan(dt)
        if isnan(t_min) && isnan(t_max)
            t_min = 0;
            t_max = (length(s) - 1) * dt;
        elseif isnan(t_max)
            t_max = ((length(s) - 1) * dt) + t_min;
        elseif isnan(t_min)
            t_min = t_max - ((length(s) - 1) * dt);
        end
    else
        if isnan(t_max)
            t_max = t_min + length(s) - 1;
        elseif isnan(t_min)
            t_min = t_max - length(s) + 1;
        end
        dt = (t_max - t_min) / (length(s) - 1);
    end

	t_range::FloatRange{Float64} = t_min:dt:t_max;

    @assert (length(t_range) == length(s)) "Invalid time specification."

    t_process_min::Float64 = t_min;
    t_process_max::Float64 = t_max;
    t_process_dt::Float64 = dt;

    if haskey(opts, "t_process_min");  t_process_min = opts["t_process_min"];  end;
    if haskey(opts, "t_process_max");  t_process_max = opts["t_process_max"];  end;
    if haskey(opts, "t_process_dt");   t_process_dt  = opts["t_process_dt"];   end;

	t_process::FloatRange{Float64} = t_process_min:t_process_dt:t_process_max;

	@assert (t_process ⊆ t_range) "Invalid time range to process."

	di_t::Int64 = int64(t_process_dt / dt);

	ω_range::FloatRange{Float64} = ω_min:float64(dω):ω_max;

	τ_width::Int64 = length(s);
	τ_length::Int64 = τ_width * 2;
	τ_min::Int64 = -τ_width;
	τ_max::Int64 = τ_width;
	τ_range::UnitRange{Int64} = τ_min:τ_max;
	h::Array{Complex{Float64},1} = ones(τ_length + 1) + 0im;

	if haskey(opts, "window_length");
		τ_length = int64(opts["window_length"]);
		τ_width = (τ_length - 1) / 2.;
		τ_min = -int64(ceil(τ_width));
		τ_max = int64(floor(τ_width));
		τ_range = τ_min:τ_max;
		h_temp::Array{Complex{Float64},1} = (0.5 * (1 - cos((2 * π * [0:τ_length]) / τ_length)));
		if !haskey(opts, "window_analytic") || opts["window_analytic"] === true
			h_temp = analytic(h_temp);
		end
		h = zeros(τ_length + 1) + 0im;
		for τ = τ_range
			h[τ + (-τ_min) + 1] = h_temp[τ + (-τ_min) + 1] * conj(h_temp[-τ + (-τ_min) + 1]);
		end
	end

	ν_length::Int64 = 1;
	ν_width::Int64 = 0;
	ν_min::Int64 = 0;
	ν_max::Int64 = 0;
	g::Array{Float64,1} = ones(length(ν_length));

	if haskey(opts, "smooth_length");
		ν_length = int64(opts["smooth_length"]);
		ν_width = (ν_length - 1) / 2.;
		ν_min = -int64(ceil(ν_width));
		ν_max = int64(floor(ν_width));
		g = (0.5 * (1 - cos((2 * π * [0:ν_length]) / ν_length)));
	end

	W::Array{Complex{Float64},2} = zeros(length(t_process), length(ω_range)) + 0im;
	
	i_t::Int64 = findfirst(t_range, t_process[1]);
	i_t_process::Int64 = 1;

	s_length::Int64 = length(s);

	for t::Float64 = t_process
		i_ω::Int64 = 1;
		for ω::Float64 = ω_range
			f_vals::Array{Complex{Float64},1} = zeros(τ_length + 1) * 0im;
			f_exp::Array{Complex{Float64},1} = exp(-im * (2 * τ_range * dt) * 2 * π * ω)
			for τ::Int64 = τ_range
				i_τ::Int64 = τ + (-τ_min) + 1;
				for ν::Int64 = max(ν_min, abs(τ) - i_t + 1):min(ν_max, s_length - abs(τ) - i_t)
					f_vals[i_τ] += conj(s[i_t + ν - τ]) * s[i_t + ν + τ] *
									g[ν + (-ν_min) + 1];
				end
				#println(i_τ)
				f_vals[i_τ] *= h[i_τ];
				f_vals[i_τ] *= f_exp[i_τ];
			end
			W[i_t_process, i_ω] = sum(f_vals);
			i_ω += 1;
		end
		i_t += di_t;
		i_t_process += 1;
	end

	return (W, t_process, ω_range);
end
