#################################################

cd("C:\\Research\\git\\tfa-julia")
cd(string(homedir(), "/Research/git/tfa-julia/"))

#################################################

include("..\\plotly-julia\\load.jl");
include("../plotly-julia/load.jl");

T_MIN = -5;
T_MAX = -T_MIN;

a = [(T_MIN - 0.5):0.01:(T_MAX + 0.5)];

f = function(t)
	if t < T_MIN
		0;
	elseif t < 0
		2;
	elseif t <= T_MAX
		4 + t / 2;
	else
		0;
	end
end

s = function(t)
	if t < T_MIN
		0;
	elseif t < 0
		sin(t * 2 * 2 * pi);
	elseif t <= T_MAX
		sin(t * (4 + t / 2) * 2 * pi);
	else
		0;
	end
end

s = function(t)
	α = 1
	β = 5
	ω0 = 10
	return ((α/π)^(1/4)) * exp((-α*(t^2)/2)+(im * β * (t^2) / 2) + (im * ω0 * t))
end

s = function(t)
	f = 5 + (t/10);
	return sin(2 * π * t * f);
end

s = function(t)
	if t < T_MIN || t > T_MAX
		return 0
	else
		f = (cos(t/5) * 10);
		return sin(2 * π * t * f);
	end
end

b = zeros(length(a)) + im;
for i = 1:length(a)
	b[i] = s(a[i]);
end

plot(a, real(b))

#################################################

include("wigner.jl");

(W, T, F) = wigner(s, {"t_min" => T_MIN, "t_max" => T_MAX})

image(T, F, real(W))

image(T, F, abs(W))

plot(T, sum(real(W), 2)')
plot(F, sum(real(W), 1))

#################################################

include("spectrogram.jl");

(W, T, F) = spectrogram(s, {"t_min" => T_MIN, "t_max" => T_MAX})

image(T, F, abs(W))

#################################################

# Analytic function equivalent

s = function(t)
	if t < T_MIN || t > T_MAX
		0 + 0im;
	else
		if t < 0
			f = 2;
		elseif t <= T_MAX
			f = 4 + t / 2;
		end
		sin(t * f * 2 * pi) + (im * -cos(t * f * 2 * pi));
	end
end

#################################################

a = [(T_MIN - 0.5):0.01:(T_MAX + 0.5)];

b = zeros(length(a)) + im;
for i = 1:length(a)
	b[i] = s(a[i]);
end

(W, T, F) = spectrogram(b, {"t_min" => a[1], "t_max" => a[length(a)], "fft_window_size" => 200})

image(T, F, abs(W))

(W, T, F) = wigner(b, {"t_min" => a[1], "t_max" => a[length(a)]})

image(T, F, real(W))

#################################################

include("analytic.jl");

c = analytic(b)

(W, T, F) = spectrogram(c, {"t_min" => a[1], "t_max" => a[length(a)], "fft_window_size" => 200})

image(T, F, abs(W))

(W, T, F) = wigner(c, {"t_min" => a[1], "t_max" => a[length(a)]})

image(T, F, real(W))

#################################################

(W, T, F) = wigner(c, {"t_min" => a[1], "t_max" => a[length(a)], "window_length" => (0.5 / 0.01) + 1, "window_analytic" => false})

image(T, F, real(W))

(W, T, F) = wigner(c, {"t_min" => a[1], "t_max" => a[length(a)], "window_length" => (2 / 0.01) + 1, "window_analytic" => false})

image(T, F, real(W))

(W, T, F) = wigner(c, {"t_min" => a[1], "t_max" => a[length(a)], "window_length" => (2 / 0.01) + 1, "window_analytic" => true})

image(T, F, real(W))

#################################################

(W, T, F) = wigner(c, {"t_min" => a[1], "t_max" => a[length(a)], "window_length" => (2 / 0.01) + 1, "window_analytic" => false, "dω" => 0.25, "smooth_length" => 11})

image(T, F, real(W))

#################################################

(W, T, F) = wigner(c, {"t_min" => a[1], "t_max" => a[length(a)], "window_length" => (2 / 0.01) + 1, "window_analytic" => false, "dω" => 0.25, "t_process_min" => -4, "t_process_max" => 4, "t_process_dt" => 0.5})

image(T, F, real(W))

#################################################

a = [-2:0.01:-0.5];

b = zeros(length(a)) + im;
for i = 1:length(a)
	b[i] = s(a[i]);
end

tic()
(W, T, F) = wigner(b, {"t_min" => a[1], "t_max" => a[length(a)], "window_length" => (0.5 / 0.01) + 1, "window_analytic" => false, "dω" => 0.25, "smooth_length" => 11})
toc()

image(T, F, real(W))

c = analytic(b)

tic()
(W, T, F) = wigner(c, {"t_min" => a[1], "t_max" => a[length(a)], "window_length" => (2 / 0.01) + 1, "window_analytic" => false, "smooth_length" => 51})
toc()

image(T, F, real(W))

#################################################

include("wigner_int_test.jl");

s = function(t)
	if t < T_MIN
		0;
	elseif t <= T_MAX
		sin(t * (6 + t / 2) * 2 * pi);
	else
		0;
	end
end

(τ_range, τ_vals, f_pos_vals, f_neg_vals, exp_vals) = wigner_int_test(s, {"t_min" => T_MIN, "t_max" => T_MAX, "τ_min" => -1, "τ_max" => 1}, 0.0, 6.)

vstacked_plot(τ_range, {[real(f_pos_vals) imag(f_pos_vals)] [real(f_neg_vals) imag(f_neg_vals)] [real(f_neg_vals .* f_pos_vals) imag(f_neg_vals .* f_pos_vals)] [real(exp_vals) imag(exp_vals)] real(τ_vals)})

sum(real(τ_vals))

(τ_range, τ_vals, f_pos_vals, f_neg_vals, exp_vals) = wigner_int_test(s, {"t_min" => T_MIN, "t_max" => T_MAX, "τ_min" => -1, "τ_max" => 1}, 0.0, 8.)

vstacked_plot(τ_range, {[real(f_pos_vals) imag(f_pos_vals)] [real(f_neg_vals) imag(f_neg_vals)] [real(f_neg_vals .* f_pos_vals) imag(f_neg_vals .* f_pos_vals)] [real(exp_vals) imag(exp_vals)] real(τ_vals)})

sum(real(τ_vals))

(τ_range, τ_vals, f_pos_vals, f_neg_vals, exp_vals) = wigner_int_test(s, {"t_min" => T_MIN, "t_max" => T_MAX, "τ_min" => -1, "τ_max" => 1}, 2.0, 6.)

vstacked_plot(τ_range, {[real(f_pos_vals) imag(f_pos_vals)] [real(f_neg_vals) imag(f_neg_vals)] [real(f_neg_vals .* f_pos_vals) imag(f_neg_vals .* f_pos_vals)] [real(exp_vals) imag(exp_vals)] real(τ_vals)})

sum(real(τ_vals))

(τ_range, τ_vals, f_pos_vals, f_neg_vals, exp_vals) = wigner_int_test(s, {"t_min" => T_MIN, "t_max" => T_MAX, "τ_min" => -1, "τ_max" => 1}, 2.0, 8.)

vstacked_plot(τ_range, {[real(f_pos_vals) imag(f_pos_vals)] [real(f_neg_vals) imag(f_neg_vals)] [real(f_neg_vals .* f_pos_vals) imag(f_neg_vals .* f_pos_vals)] [real(exp_vals) imag(exp_vals)] real(τ_vals)})

sum(real(τ_vals))

s = function(t)
	if t < T_MIN
		0;
	elseif t <= T_MAX
		sin(t * 4 * 2 * pi);
	else
		0;
	end
end

(τ_range, τ_vals, f_pos_vals, f_neg_vals, exp_vals) = wigner_int_test(s, {"t_min" => T_MIN, "t_max" => T_MAX, "τ_min" => -1, "τ_max" => 1}, 0.0, 4.)

vstacked_plot(τ_range, {[real(f_pos_vals) imag(f_pos_vals)] [real(f_neg_vals) imag(f_neg_vals)] [real(f_neg_vals .* f_pos_vals) imag(f_neg_vals .* f_pos_vals)] [real(exp_vals) imag(exp_vals)] real(τ_vals)})

sum(real(τ_vals))
