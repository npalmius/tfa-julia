# Copyright (c) 2014, Nick Palmius (University of Oxford)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the University of Oxford nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Contact: npalmius@googlemail.com
# Originally written by Nick Palmius, 5-Oct-2014

function wigner_int_test(f::Function)
	wigner_int_test(f, Dict(), 0., 1.);
end

function wigner_int_test(f::Function, opts::Dict, t::Float64, ω::Float64)
	t_min = -5;
	t_max = 5;
	t_diff = t_max - t_min
	τ_min = -t_diff;
	τ_max = t_diff;
	dτ = 0.01;

    # Process options
    if haskey(opts, "τ_min");  τ_min = opts["τ_min"];  end;
    if haskey(opts, "τ_max");  τ_max = opts["τ_max"];  end;
    if haskey(opts, "dτ");     dτ    = opts["dτ"];     end;

	τ_range = τ_min:dτ:τ_max;

	τ_vals = zeros(length(τ_range)) + 0im;
	f_pos_vals = zeros(length(τ_range)) + 0im;
	f_neg_vals = zeros(length(τ_range)) + 0im;
	exp_vals = zeros(length(τ_range)) + 0im;

	k = 1;
	for τ = τ_range
		f_pos_vals[k] = f(t + (τ/2));
		f_neg_vals[k] = conj(f(t - (τ/2)));
		exp_vals[k] = exp(-im * τ * 2 * π * ω);
		τ_vals[k] = conj(f(t - (τ/2))) * f(t + (τ/2)) * exp(-im * τ * 2 * π * ω)
		k += 1;
	end

	return (τ_range, τ_vals, f_pos_vals, f_neg_vals, exp_vals);
end
